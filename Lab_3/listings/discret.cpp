#include <stdio.h>
#include <math.h>
#define ROW 15
#define COL 15
#define WINDOW_SIZE 3

void PrintMatrix(FILE* file, int* matrix[], const char* name) {
	fprintf(file, "======= Matrix %s =======\n", name);
	for(int i = 0; i < ROW; i++) {
		for(int j = 0; j < COL; j++)
			fprintf(file, "%d\t", matrix[i * COL + j]);
		
	    fprintf(file, "\n");
	}
	fprintf(file, "\n\n");
}


void main(void) {
	const int signal_noise[15][15] = {
		{0 , 12 , 34 , 40 , 62 , 86 , 109, 144, 158, 147, 115, 126, 94 , 71 , 42 },
		{12, 31 , 51 , 78 , 89 , 141, 202, 197, 224, 217, 173, 171, 135, 85 , 59 },
		{0 , 8  , 35 , 56 , 110, 173, 204, 269, 264, 277, 258, 212, 183, 99 , 85 },
		{14, 35 , 64 , 103, 152, 238, 330, 409, 370, 436, 387, 313, 288, 142, 101},
		{0 , 16 , 50 , 94 , 171, 262, 341, 365, 413, 379, 435, 368, 256, 141, 92 },
		{32, 78 , 120, 150, 221, 700, 390, 491, 475, 437, 397, 395, 261, 191, 60 },
		{16, 51 , 145, 197, 290, 399, 431, 587, 550, 609, 449, 463, 237, 204, 60 },
		{56, 118, 202, 275, 360, 510, 659, 826, 699, 676, 558, 571, 328, 271, 85 },
		{16, 40 , 106, 198, 298, 428, 472, 618, 542, 577, 700, 369, 261, 220, 70 },
		{40, 62 , 105, 150, 256, 383, 519, 509, 457, 426, 469, 327, 260, 110, 50 },
		{20, 30 , 96 , 166, 261, 275, 404, 395, 458, 335, 393, 246, 241, 110, 40 },
		{48, 88 , 120, 173, 214, 228, 271, 350, 298, 324, 183, 201, 146, 110, 25 },
		{40, 52 , 139, 131, 160, 195, 281, 306, 175, 198, 140, 184, 92 , 60 , 0  },
		{32, 58 , 70 , 130, 144, 199, 231, 209, 200, 173, 168, 103, 80 , 60 , 25 },
		{16, 28 , 50 , 78 , 91 , 119, 146, 169, 149, 137, 107, 79 , 85 , 55 , 25 },
		};

	int filter[3][3] = {{3, 2, 3},
						{2, 1, 2},
						{3, 2, 3}};
	const int amount = 21;
	const double m = 1.25;
	double nu = 0.0;
	int signal[15][15];
	int delta[15][15];
	
	FILE* file = fopen("rash3.csv", "w");
	// anisotropic filter
	for(int i = 0; i < ROW; i++)
		for(int j = 0; j < COL; j++) {
			signal[i][j] = 0;
			delta[i][j] = 0;
		}

	for(int i = 0; i < ROW; i++)
		for(int j = 0; j < COL; j++) {
			if(i == 0 || i == ROW - 1 || j == 0 || j == COL - 1)
				signal[i][j] = signal_noise[i][j];
			else {
				int sg = 0;
				for(int k1 = 0; k1 < WINDOW_SIZE; k1++) 
					for(int k2 = 0; k2 < WINDOW_SIZE; k2++) 
						sg += filter[k1][k2] * signal_noise[i - 1 + k1][j - 1 + k2];
				signal[i][j] = sg / amount;
			}
		}

	for(int i = 0; i < ROW; i++) 
		for(int j = 0; j < COL; j++) 
			delta[i][j] = signal_noise[i][j] - signal[i][j];


	fprintf(file, "Anisotropic filter\n\n");
	PrintMatrix(file, (int**)signal_noise, "Signal-noise");
	PrintMatrix(file, (int**)signal, "Signal");
	PrintMatrix(file, (int**)delta, "Delta (Signal-noise - Signal)");
	fprintf(file, "\n\n");
	// statistic filter (nu = m * sigma)
	for(int i = 0; i < ROW; i++) 
		for(int j = 0; j < COL; j++) {
			signal[i][j] = 0;
			delta[i][j] = 0;
		}
	
	for(int i = 0; i < ROW; i++) {
		for(int j = 0; j < COL; j++) {
			int
			sum1 = 0;
			double sum2 = 0.0;
			for(int k1 = 0; k1 < WINDOW_SIZE; k1++) 
				for(int k2 = 0; k2 < WINDOW_SIZE; k2++)
					sum1 += signal_noise[i - 1 + k1][j - 1 + k2];

			double G = (double)sum1 / pow((double)WINDOW_SIZE, 2);
			for(int k1 = 0; k1 < WINDOW_SIZE; k1++)
				for(int k2 = 0; k2 < WINDOW_SIZE; k2++)
					sum2 += pow((double)signal_noise[i - 1 + k1][j - 1 + k2] - G, 2);

			double D = sum2 / (pow((double)WINDOW_SIZE, 2) - 1);
			nu = m * sqrt(D);
			signal[i][j] = (signal_noise[i][j] - G) < nu ? signal_noise[i][j] : (int)G;
		}
	}
	for(int i = 0; i < ROW; i++)
		for(int j = 0; j < COL; j++)
			delta[i][j] = signal_noise[i][j] - signal[i][j];

	fprintf(file, "Statistic filter\n\n");
	PrintMatrix(file, (int**)signal_noise, "Signal-noise");
	PrintMatrix(file, (int**)signal, "Signal");
	PrintMatrix(file, (int**)delta, "Delta (Signal-noise - Signal)");
	fprintf(file, "\n\n");
	fclose(file);
}
