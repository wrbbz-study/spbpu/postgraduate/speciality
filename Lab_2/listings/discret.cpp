#include <stdio.h>

#define H_ROW 8
#define H_COL 8
#define X_ROW 8
#define X_COL 8
#define Y_ROW (H_ROW + X_ROW - 1)
#define Y_COL (H_COL + X_COL - 1)

bool PrintMatrix(FILE* file, int* matrix[], char type) {
	int row, col;
	switch (type) {
	case 'H':
		row = H_ROW;
		col = H_COL;
		break;
	case 'X':
		row = X_ROW;
		col = X_COL;
		break;
	case 'Y':
		row = Y_ROW;
		col = Y_COL;
		break;
	default:
		return false;
	}
	fprintf(file, "======= Matrix %c(%d, %d)=======\n", type, row, col);
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++)
			fprintf(file, "%d\t", matrix[i*col + j]);
		fprintf(file, "\n");
	}
	fprintf(file, "\n\n");
	
	
	return true;
}

int main(void) {
	int h[H_ROW][H_COL] = {
	{ 0, 4, 6, 0, 6, 4, 5, 6 },
	{ 4, 5, 5, 6, 0, 5, 5, 5 },
	{ 0, 0, 5, 4, 0, 7, 0, 5 },
	{ 2, 3, 4, 4, 4, 7, 7, 8 },
	{ 0, 0, 0, 4, 5, 4, 3, 5 },
	{ 4, 0, 0, 0, 6, 0, 5, 0 },
	{ 0, 5, 7, 6, 0, 0, 7, 5 },
	{ 8, 6, 7, 7, 0, 7, 6, 5 }};

	int x[X_ROW][X_COL] = {
	{ 3, 4, 4, 5, 5, 6, 6, 7 },
	{ 0, 0, 0, 0, 7, 4, 0, 4 },
	{ 2, 2, 3, 4, 4, 4, 4, 5 },
	{ 0, 0, 0, 5, 6, 6, 0, 0 },
	{ 4, 4, 5, 0, 5, 0, 7, 0 },
	{ 4, 0, 7, 0, 5, 0, 7, 0 },
	{ 4, 3, 0, 5, 5, 5, 0, 0 },
	{ 2, 2, 3, 4, 4, 4, 5, 5 }};

	int y[Y_ROW][Y_COL];

	for (int i = 0; i < Y_ROW; i++)
		for (int j = 0; j < Y_COL; j++)
			y[i][j] = 0;

	for (int i = 0; i < Y_ROW; i++)
		for (int j = 0; j < Y_COL; j++)
			for (int k1 = 0; k1 < H_ROW; k1++)
				for (int k2 = 0; k2 < H_COL; k2++)
					if ((i - k1 >= 0) && (i - k1 < X_ROW) && (j - k2 < X_COL) && (j - k2 >= 0))
						y[i][j] += h[k1][k2] * x[i - k1][j - k2];

	FILE* file = fopen("method_1.csv", "w");
	if (file != NULL) {
		PrintMatrix(file, (int**)h, 'H');
		PrintMatrix(file, (int**)x, 'X');
		PrintMatrix(file, (int**)y, 'Y');
		fclose(file);
	}
}
