#include <stdio.h>

#define H_ROW 8
#define H_COL 8
#define X_ROW 8
#define X_COL 8
#define Y_ROW (H_ROW + X_ROW - 1)
#define Y_COL (H_COL + X_COL - 1)

void PrintMatrix(FILE* file, int* matrix[], const char* type) {
	int row, col;
	if (type == "H") {
		row = H_ROW;
		col = H_COL;
	} else if (type == "X") {
		row = X_ROW;
		col = X_COL;
	} else {
		row = Y_ROW;
		col = Y_COL;
	}

	fprintf(file, "======= Matrix %s(%d, %d)=======\n", type, row, col);

	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++)
			fprintf(file, "%d\t", matrix[i*col + j]);
		fprintf(file, "\n");
	}
	fprintf(file, "\n\n");
}

int main(void) {

		int h[H_ROW][H_COL] = {
	{ 0, 4, 6, 0, 6, 4, 5, 6 },
	{ 4, 5, 5, 6, 0, 5, 5, 5 },
	{ 0, 0, 5, 4, 0, 7, 0, 5 },
	{ 2, 3, 4, 4, 4, 7, 7, 8 },
	{ 0, 0, 0, 4, 5, 4, 3, 5 },
	{ 4, 0, 0, 0, 6, 0, 5, 0 },
	{ 0, 5, 7, 6, 0, 0, 7, 5 },
	{ 8, 6, 7, 7, 0, 7, 6, 5 }};

	int x[X_ROW][X_COL] = {
	{ 3, 4, 4, 5, 5, 6, 6, 7 },
	{ 0, 0, 0, 0, 7, 4, 0, 4 },
	{ 2, 2, 3, 4, 4, 4, 4, 5 },
	{ 0, 0, 0, 5, 6, 6, 0, 0 },
	{ 4, 4, 5, 0, 5, 0, 7, 0 },
	{ 4, 0, 7, 0, 5, 0, 7, 0 },
	{ 4, 3, 0, 5, 5, 5, 0, 0 },
	{ 2, 2, 3, 4, 4, 4, 5, 5 }};

	int temp[Y_ROW][Y_COL];
	int y[Y_ROW][Y_COL];

	for (int i = 0; i < Y_ROW; i++)
		for (int j = 0; j < Y_COL; j++)
			y[i][j] = 0;

	FILE* file = fopen("method_2.xls", "w");
	PrintMatrix(file, (int**)h, "H");
	PrintMatrix(file, (int**)x, "X");

	for (int k1 = 0; k1 < X_ROW; k1++)
		for (int k2 = 0; k2 < X_COL; k2++) {
			for (int i = 0; i < Y_ROW; i++)
				for (int j = 0; j < Y_COL; j++)
					temp[i][j] = 0;

			for (int m1 = 0; m1 < H_ROW; m1++)
				for (int m2 = 0; m2 < H_COL; m2++)
					temp[m1 + k1][m2 + k2] = h[m1][m2] * x[k1][k2];

			char matr[12] = "";
			sprintf(matr, "Temp_%d_%d", k1 + 1, k2 + 1);
			PrintMatrix(file, (int**)temp, matr);
			for (int i = 0; i < Y_ROW; i++)
				for (int j = 0; j < Y_COL; j++)
					y[i][j] += temp[i][j];
	}

	PrintMatrix(file, (int**)y, "Y");
	fclose(file);
}
